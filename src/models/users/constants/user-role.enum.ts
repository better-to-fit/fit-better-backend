export enum UserRole {
    Moderator = 'Moderator',
    Admin = 'Admin',
    Basic = 'Basic'
}
