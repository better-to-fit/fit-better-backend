export enum UserGender {
    Male = 'Male',
    Female = 'Female',
    NoGender = 'Prefer not to say'
}
