import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from "@nestjs/common";
import { DataBaseModule } from './../../data/database/database.module';
import { UsersController } from "./users.controller";
import { UsersService } from './users.service';
import { User } from 'src/data/entities/user.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User
          ]),
          DataBaseModule,
    ],
    controllers: [UsersController],
    providers: [UsersService]
})
export class UsersModule {}
