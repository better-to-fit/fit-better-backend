import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DataBaseModule } from './data/database/database.module';
import { CoreModule } from './data/database/core.module';
import { User } from './data/entities/user.entity';
@Module({
  imports: [
    DataBaseModule,
    CoreModule,
    TypeOrmModule.forFeature([User]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
