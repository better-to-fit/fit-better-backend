import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { UserGender } from "src/models/users/constants/user-gender.enum";
import { UserRole } from "src/models/users/constants/user-role.enum";

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column({ type: 'nvarchar', nullable: false })
    username: string;

    @Column({ type: 'nvarchar', nullable: false })
    password: string;

    @Column({ type: 'nvarchar', nullable: false })
    firstName: string;

    @Column({ type: 'nvarchar', nullable: false })
    lastName: string;

    @Column({ type: 'nvarchar', nullable: false, unique: true })
    email: string;

    @Column({ type: 'enum', enum: UserGender, default: UserGender.NoGender, nullable: false })
    gender: string;

    @Column({ type: 'enum', enum: UserRole, default: UserRole.Basic, nullable: false })
    userRole: string;

    @Column({ type: 'nvarchar', default: null })
    avatarUrl: string;

    @CreateDateColumn({ type: 'timestamp', default: null })
    createdOn: Date;

    @UpdateDateColumn({ type: 'timestamp', default: null })
    updatedOn: Date;

    @DeleteDateColumn({ type: 'timestamp', default: null })
    deletedOn: Date;

    @Column({ type: 'boolean', default: false })
    isDeleted: boolean;
}
