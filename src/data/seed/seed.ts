/* eslint-disable no-console */
import { createConnection } from 'typeorm';

const seed = async() => {
  console.log('Seed started!');
  const connection = await createConnection();

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
